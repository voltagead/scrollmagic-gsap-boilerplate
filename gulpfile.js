var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('default', defaultTask);
function defaultTask(done) {
	// place code for your default task here
	// NOT USED
	done();
}

gulp.task('sass', function(){
	return gulp.src('scss/site.scss')
		.pipe(sass()) // Using gulp-sass
		.pipe(gulp.dest('css/'))
});

gulp.task('watch', function(){
//	gulp.watch('scss/*.scss', ['sass']); // old version
	gulp.watch('scss/*.scss', gulp.series('sass')); // new version
});