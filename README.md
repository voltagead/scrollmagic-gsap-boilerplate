**Boilerplate for Scroll-Based Animation and Interactivity**

---

## ScrollMagic

ScrollMagic is a JavaScript scroll-based interaction library.

http://scrollmagic.io/
http://scrollmagic.io/docs/
http://scrollmagic.io/examples/
http://scrollmagic.io/docs/animation.GSAP.html
https://ihatetomatoes.net/scrollmagic-cheat-sheet/#more-12764


ScrollMagic can be used on its own for many uses, or with other animation libraries, but it is frequently used to trigger and control animation created with GSAP

---

## GreenSock Animation Platform (GSAP)

GSAP is a JS animation library.

https://greensock.com/gsap
https://greensock.com/docs/
https://greensock.com/docs/Plugins/CSSPlugin
https://greensock.com/docs/Easing
https://ihatetomatoes.net/greensock-cheat-sheet/

---

## Purpose of this repo

This repo serves as a starting point for any projects requiring scroll-based animation or interactivity.

1. Examples of techniques
2. Best practices
3. Architecture


## Documentation and Tips

1. For each section or object to animate that shares the same trigger and duration, create a ScrollMagic scene.

2. Scrollmagic methods that can be used without GSAP for scroll-based durations include "pinning" (emulating fixed position), class triggers (setting and removing CSS classes), and any other JS-based functionality that needs to run on enter or leave with optional direction-specific detection.

3. Part of the learning curve is that there are often many different ways to organize the code to accomplish the same result.

4. Key concepts of ScrollMagic scenes:		http://scrollmagic.io/docs/ScrollMagic.Scene.html#constructor
    A. triggerElement = What element is triggering the animation
        *note that you would not want an element being animated with motion to be the trigger for itself
        *even if jQuery is not used, SM accepts basic CSS jQuery-like selectors. A jQuery plugin is available if more advanced selector methods are needed.
    B. triggerHook = Where the trigger starts, based on the triggerElement, in relation to the viewport.
        options: "onEnter", "onCenter" (default), "onLeave", or numbered values from 0-1 such as 0.5 which is the same as "onCenter"
    C. duration = Duration of the scene.
        Default is 0, which will auto-play when triggered.
        Integers are interpreted as pixels.
        String values are used to specifiy percentage (of viewport height) or negative values.
        Also accepts functions returning numeric values.
        Note that there is no built-in method to tie a duration to an element's height; you would need to build that yourself.
    D. offset = optional offset for trigger position, up (positive value) or down (negative value). Default is 0.

5. For an individual animation, create a "tween" with TweenMax.

6. A single scene can trigger multiple tweens, either simultaneiously or sequentially.

7. Tweens define the animated properties and whether these properties are to be the end point of an animation (using "to"), or the starting point (using "from"). You can also define both ends using "fromTo".

8. For more intricate animation, a GSAP "timeline" can be used as a TimelineMax instance, instead of a single tween. Timelines can contain multiple tweens, and they can be controlled with many useful methods such as being staggered automatically, looping and repeating, and reversed.

9. Most GSAP examples use animatable properties based on GSAP's "CSSPlugin" which is included in TweenMax. This offers many shorthand and helper methods, and can interpret and animate properties that aren't directly animatable in CSS. These CSSPlugin properties are written in camel case; some examples: "autoAlpha, scaleX, rotationY, boxShadow".

10. It's sometimes necessary to disable scroll-based animation on mobile devices due to performance issues or having layout styling that's too different from desktop to use the same animation methods. Though you can spend the effort to get close with various checks and rechecks on resize, there isn't a clean way to seamlessly go back and forth between "enable on desktop" and "disable on mobile". So your best bet is to initially detect screen size and enable or disable from there. You can check the screen size with a throttled on resize function, then force a reload of the page if you hit a breakpoint that requires different functionality. Since any animation that GSAP adds is added inline, you can override those styles or strip out the style attributes carefully with JS if you need to. It can take a lot of time to fine-tune and test any conditional responsive, fallback, or device-specific variations. These techniques are not detailed in this repo, they are usually very site-specific.
