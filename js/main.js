document.addEventListener( 'DOMContentLoaded' , function() {

	// debug trigger for testing
	var doDebug = true;

	// init SM controller
	var controller = new ScrollMagic.Controller();

	// global easing method
	var globalEase = 'Power1.easeOut';

	// Smooth Scrolling using GSAP scrollTo plugin
	controller.scrollTo( function ( newpos ) {
		TweenMax.to( window, 1, { scrollTo: { y: newpos } } );
	});

	var scrollNavLinks = document.getElementsByClassName( 'js-site-nav-link' );
	for ( var i = 0; i < scrollNavLinks.length; i++ ) {
		scrollNavLinks[i].addEventListener( 'click', function(e) {
			e.preventDefault();

			// trigger scroll to links' href targeting IDs
			controller.scrollTo( this.getAttribute( 'href' ) );
		});
	}

	/*** Set up ScrollMagic (SM) "scenes" and add GSAP timelines and tweens to scenes. ***
		see README for intro and reference documentation links
	*/


	// *** Scene 1 **************************************************
	// Basic SM scene with tween defined inline and chained, added to SM controller.
	var scene1 = new ScrollMagic.Scene({
		triggerElement: '.site-main',
			// Since the target of this animation is being told to move vertically, the trigger element must be a normally positioned element containing the target.
		duration: '100%',
			// 100% of the height of the viewport. If No duration is specified, the animation is simply triggered and the Tween's duration is used.
		triggerHook: 'onLeave'
			// Trigger the animation when the triggerElement leaves the viewport. Thus, the start trigger is at the very top of the screen since the specified triggerElement contains all content on the page.
			// TriggerHooks can also be specified as 0-1 for % of viewport height; thus 'onLeave' = 1, 'onEnter' = 0, 'onCenter' = 0.5. For a triggerHook 75% of the height of the viewport from the top = 0.75.
	}).setTween( TweenMax.to( '.section1', 1, { y: '50%' } ) ).addTo( controller );
		// A single tween is set on the scene, telling the '.section1' element to animate its transform: translateY value to 50%. The tween duration of '1' could be any value above 0, as the duration is being controlled by SM. The effect of telling the element to move down 50% while its container is scrolling up is to appear moving up more *slowly* than the container.
		// Performance tip: animate motion using "x" and "y" properties if possible, which use transform: translate. Animating with "top" or "left" uses those CSS absolute positioning properties that cause more browser rendering repainting. Note that x and y values would be relative to the size of the object itself, per CSS transform spec, while top and left are relative to the object's relative/absolute/fixed positioned parent.
		// Lastly, don't forget to add this SM scene with its tween to the SM controller.


	// *** Scene 2 **************************************************
	// Animate a couple elements using a Timeline
	var scene2 = new ScrollMagic.Scene({
		triggerElement: '.section2',
		duration: '100%'
		// no triggerHook specified, so use default (onCenter)
	});

	// new TimelineMax instance
	var scene2Timeline = new TimelineMax();

	// tween with 2 properties
	var scene2Tween1 = TweenMax.to( '.section2-object1', 2, { left: '100%', rotation: 360 } );

	// tween with 2 properties - autoAlpha sets visibility to improve performance at opacity 0 or 1
	var scene2Tween2 = TweenMax.to( '.section2-object2', 3, { autoAlpha: 0, scale: 4 } );

	// another tween with 2 properties - using fromTo which sets both beginning and end properties
	var scene2Tween3 = TweenMax.fromTo( '.section2-object3', 2, { autoAlpha: 0, rotationX: '-90' }, { autoAlpha: 1, rotationX: 0 } );

	// add 1st 2 tweens to timeline and run simultaneously, then add 3rd tween which will run sequentially after 1st 2 tweens
	scene2Timeline.add([ scene2Tween1, scene2Tween2 ]).add( scene2Tween3 );

	// add timeline to scene, add scene to controller
	scene2.setTween( scene2Timeline ).addTo( controller );


	// *** Scene 2.1 **************************************************
	// Animate a couple elements using a Timeline
	var scene2_1 = new ScrollMagic.Scene({
		triggerElement: '.section2_1',
		// Note that no duration: was specified, so the Tween/Timeline is triggered on scene enter without its progress being tied to the scroll position
		// no triggerHook specified, so use default (onCenter)
	});

	// new TimelineMax instance
	var scene2_1Timeline = new TimelineMax();

	// tween with 2 properties
	var scene2_1Tween1 = TweenMax.to( '.section2_1-object1', 2, { left: '100%', rotation: 360 } );

	// tween with 2 properties - autoAlpha sets visibility to improve performance at opacity 0 or 1
	var scene2_1Tween2 = TweenMax.to( '.section2_1-object2', 3, { autoAlpha: 0, scale: 4 } );

	// another tween with 2 properties - using fromTo which sets both beginning and end properties
	var scene2_1Tween3 = TweenMax.fromTo( '.section2_1-object3', 2, { autoAlpha: 0, rotationX: '-90' }, { autoAlpha: 1, rotationX: 0 } );

	// add 1st 2 tweens to timeline and run simultaneously, then add 3rd tween which will run sequentially after 1st 2 tweens
	scene2_1Timeline.add([ scene2_1Tween1, scene2_1Tween2 ]).add( scene2_1Tween3 );

	// add timeline to scene, add scene to controller
	scene2_1.setTween( scene2_1Timeline ).addTo( controller );


	// *** Scene 3 **************************************************
	// Pin an element

	var scene3 = new ScrollMagic.Scene({
		triggerElement: '.section3',
		duration: '75%',
		triggerHook: 'onLeave'
	})
		.setPin( '.section3-object1', { pushFollowers: false } )
		// setPin emulates fixed positioning but is triggered on and off. By disabling pushFollowers, artificial spacing added below pinned element is disabled
		.setClassToggle( '.section3-object2, .section3-fixedbg', 'js-active' )
		// setClassToggle adds/removes css classes on target during scene duration
	.addTo( controller );


	// *** Scene 4 **************************************************
	// Use a timeline to automatically stagger multiple tweens
	var scene4 = new ScrollMagic.Scene({
		triggerElement: '.section4-objects',
		duration: 400,
		offset: 100
		// this moves the trigger 100px down from where it would normally be
	});

	var scene4Timeline = new TimelineMax();
	scene4Timeline.staggerTo( '.section4-objects div', 1, { backgroundColor: '#000' }, .1 );
	scene4.setTween( scene4Timeline ).addTo( controller );


	// *** Scene 5 **************************************************
	// Control video playback using ScrollMagic scene Enter/Leave events

	var scene5Video = document.getElementsByClassName( 'section5-video' )[0];
	var scene5Text1 = document.getElementsByClassName( 'section5-text-1' )[0];
	var scene5Text2 = document.getElementsByClassName( 'section5-text-2' )[0];

	// get YouTube Video ID from HTML data attr
	var ytVideoElId = 'iframe-player',
		ytVideoEl = document.getElementById( ytVideoElId ),
		ytVideoDataId = ytVideoEl.getAttribute( 'data-video-id' );

	if ( ytVideoDataId ) {
		// YouTube iFrame API - https://developers.google.com/youtube/iframe_api_reference
		var tag = document.createElement( 'script' );
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName( 'script' )[0];
		firstScriptTag.parentNode.insertBefore( tag, firstScriptTag );
		var player;

		window.onYouTubePlayerAPIReady = function() {
			player = new YT.Player( ytVideoElId, {
				height: '340',
				width: '602',
				videoId: ytVideoDataId,
				playerVars: {
					controls: 0,
					playsinline: 1,
					rel: 0,
					modestbranding: 1
				},
				events: {
					'onReady': onPlayerReady,
					'onStateChange': onPlayerStateChange
				}
			});
		}
	}

	function onPlayerReady( event ) {
		// mute player for demo purposes
		player.mute();
	}

	function onPlayerStateChange( event ) {

		// loop video if ended
		if ( event.data == YT.PlayerState.ENDED ) {
			player.playVideo();
		}
	}

	var scene5 = new ScrollMagic.Scene({
		triggerElement: '.section5',
		duration: '100%'
	})
		.on( 'enter', function( event ) {
			// trigger JavaScript on scene enter
			scene5Text1.textContent = 'Entering scene 5';

			// trigger HTML5 video playback
			scene5Video.play();

			// trigger YouTube iframe API playback
			if ( player ) {
				player.playVideo();
			}

			// detect which direction the user is scrolling on scene enter
			if ( event.scrollDirection === 'REVERSE' ) {
				scene5Text2.textContent = 'Entering scene 5 in reverse';
			}
		})
		.on( 'leave', function( event ) {

			// pause HTML5 video
			scene5Video.pause();

			// stop YouTube video
			if ( player ) {
				player.stopVideo();
			}

			if ( event.scrollDirection === 'REVERSE' ) {
				scene5Text1.textContent = 'Leaving scene 5 in reverse';
			} else if ( event.scrollDirection === 'FORWARD' ) {
				scene5Text2.textContent = 'Leaving scene 5 forwards';
			}
		})
		.addTo( controller );


	// *** Scene 6 **************************************************
	// Reusable CSS class hooks that apply animation automatically

	// Trigger fade in on any number of elements in sequentially from bottom. uses container subclass, which can be applied to same targeting element
	$( '.js-anim--fade-Xup' ).each( function() {
		var $el = $( this );

		var $elSubContainer = $el.find( '.js-anim--fade-Xup_subs' );

		if ( !$elSubContainer.length && $el.hasClass( 'js-anim--fade-Xup_subs') ) {
			$elSubContainer = $el;
		} else if ( !$elSubContainer.length ) {
			$elSubContainer = $el.children().get(0);
		}

		var $elSubs = $elSubContainer.children();

		var scene = new ScrollMagic.Scene({
			triggerElement: this,
			triggerHook: 0.5
		});

		var timeline = new TimelineMax();

		timeline.staggerFrom( $elSubs, 0.5, { autoAlpha: 0, y: '20%', ease: globalEase }, 0.25 );

		scene.setTween( timeline );
		scene.addTo( controller );

		if ( doDebug ) {
			scene.addIndicators({ name: 'fade X up' });
		}
	});


	// *** Scene 7 **************************************************

	// reusable class hook to draw SVG strokes - requires subclasses
	$( '.js-anim--svg-draw' ).each( function() {
		var $el = $( this );
		var $elSubs = $el.find( '[class*="js-anim--svg-draw_sub"]' );
		var elSubsLength = $elSubs.length;

		var scene = new ScrollMagic.Scene({
			triggerElement: this
		});

		var timeline = new TimelineMax();

		$elSubs.each( function() {
			var $elSub = $( this );
			// drawing SVG strokes involves getting the length of the stroke, and setting the SVG stroke-dasharray and dashoffset properties to that length, then reducing them to 0
			var strokeLength = $elSub[0].getTotalLength();
			$elSub.css( 'stroke-dasharray', strokeLength );
			$elSub.css( 'stroke-dashoffset', strokeLength );
		});

		// staggerTo( target, duration of tween, { svg prop: value }, delay between each item staggered, position parameter aka initial delay of entire timeline )
		timeline.staggerTo( $elSubs, 1.5, { strokeDashoffset: 0 }, 0.25, 0.5 );

		scene.setTween( timeline );
		scene.addTo( controller );

		if ( doDebug ) {
			scene.addIndicators({ name: 'svg draw' });
		}
	});

	// trigger grow and randomize color of SVG sub-elements
	var scene7 = new ScrollMagic.Scene({
		triggerElement: '.svg-force4good'
	});

	var scene7Timeline = new TimelineMax();
	// this example shows staggerFromTo - controlling properties at both ends of the animation rather than just 'from' or 'to'
	// it also uses the special 'cycle' method to allow independently randomized color values for the fill on the 'to' end.
	scene7Timeline.staggerFromTo( '.svg-force4good path:not( .svg-force4good-bg )', 0.5, { scale: 0 }, { scale: 1, cycle: { 'fill': makeRandomColor } }, .1 );
	scene7.setTween( scene7Timeline ).addTo( controller );

	function makeRandomColor() {
		var c = '';
		while ( c.length < 7 ) {
			c += ( Math.random()).toString(16).substr(-6).substr(-1);
		}
		return '#'+c;
	}

	// *** Scene 8 **************************************************

	// Change color of nav over white bg
	// Trigger a looping animation
	var scene8 = new ScrollMagic.Scene({
		triggerElement: '.section8'
	});
	scene8.setClassToggle( '.site-nav', 'js-lightbg' );

	var scene8Timeline = new TimelineMax( {
		// repeat: -1 = infinite
		repeat: -1,
		// yoyo: true plays forwards, then reverse, then forwards, etc.
		yoyo: true,
		// adds a 1sec delay between repeats
		repeatDelay: 1
	});
	// sequence object coming in from left to center, pausing, then off to the right
	// Easing methods: https://greensock.com/docs/Easing
	scene8Timeline.from( '.svg-voltage', 1, { ease: Bounce.easeOut, x: '-200%' } );
	// '+=1' at the end of this adds delay (at current time position + 1 second)
	scene8Timeline.to( '.svg-voltage', 1, { ease: Power4.easeOut, x: 0 }, '+=1' );
	scene8Timeline.to( '.svg-voltage', 1, { ease: Power4.easeOut, x: '200%' } );
	scene8.setTween( scene8Timeline );
	scene8.addTo( controller );


	// DEBUG ***********************************************

	// show debug indicators for testing
	if ( doDebug ) {
		scene1.addIndicators();
		// show more options for name, color:
		scene2.addIndicators({
			name: 'Scene 2 Indicator options example',
			colorStart: '#00f',
			colorTrigger: '#fff',
			colorEnd: '#000'
		});
		scene2_1.addIndicators({
			name: 'Scene 2_1'
		});
		scene3.addIndicators({
			name: 'Scene 3'
		});
		scene4.addIndicators({
			name: 'Scene 4'
		});
		scene5.addIndicators({
			name: 'Scene 5',
			colorStart: '#fff'
		});
		scene7.addIndicators({
			name: 'Scene 7'
		});
		scene8.addIndicators({
			name: 'Scene 8'
		});
	}
});